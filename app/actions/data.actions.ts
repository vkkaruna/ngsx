import { Datatype } from "../model/datatype.model";

export class AddData {
    static readonly type = '[DATA] Add'

    constructor(public payload: Datatype) {}
}

export class RemoveData {
    static readonly type = '[DATA] Remove'

    constructor(public payload: string) {}
}