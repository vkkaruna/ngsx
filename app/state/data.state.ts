import { State, Action, StateContext, Selector } from '@ngxs/store';
import { Datatype } from '../model/datatype.model';
import { AddData, RemoveData } from './../actions/data.actions';

// Section 2
export class DataStateModel {
    data: Datatype[];
}

// Section 3
export class TutorialState {
    
        // Section 4
        @Selector()
        static getTutorials(state: DataStateModel) {
            return state.data
        }
    
        // Section 5
        @Action(AddData)
        add({getState, patchState }: StateContext<DataStateModel>, { payload }:AddData) {
            const state = getState();
            patchState({
                data: [...state.data, payload]
            })
        }
    
        @Action(RemoveData)
        remove({getState, patchState }: StateContext<DataStateModel>, { payload }:RemoveData) {
            patchState({
                data: getState().data.filter(a => a.name != payload)
            })
        }
    
    }