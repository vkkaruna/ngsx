import { Component, OnInit } from '@angular/core';
import { Store, Select } from '@ngxs/store';
import { Datatype } from '../model/datatype.model'
import { DataStateModel } from './../state/data.state' 
import { Observable } from 'rxjs/Observable'
import { RemoveData } from '../actions/data.actions'

@Component({
  selector: 'app-read',
  templateUrl: './read.component.html',
  styleUrls: ['./read.component.css']
})
export class ReadComponent implements OnInit {

  tutorials$: Observable<Datatype>
  
    constructor(private store: Store) {
        this.tutorials$ = this.store.select(state => state.tutorials.tutorials)
    }
  
    delTutorial(name) {
      this.store.dispatch(new RemoveData(name))
    }
  
    ngOnInit() {}
}
